import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _398ff3f5 = () => interopDefault(import('../pages/about.vue' /* webpackChunkName: "pages/about" */))
const _5d8bd26d = () => interopDefault(import('../pages/appointment/edit/_id.vue' /* webpackChunkName: "pages/appointment/edit/_id" */))
const _4222c932 = () => interopDefault(import('../pages/appointment/_id.vue' /* webpackChunkName: "pages/appointment/_id" */))
const _5947ceba = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _398ff3f5,
    name: "about"
  }, {
    path: "/appointment/edit/:id?",
    component: _5d8bd26d,
    name: "appointment-edit-id"
  }, {
    path: "/appointment/:id?",
    component: _4222c932,
    name: "appointment-id"
  }, {
    path: "/",
    component: _5947ceba,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
