import { wrapFunctional } from './utils'

export { default as Card } from '../../components/Card.vue'
export { default as Cards } from '../../components/Cards.vue'
export { default as Footer } from '../../components/Footer.vue'
export { default as NewAppointment } from '../../components/NewAppointment.vue'
export { default as Tabs } from '../../components/Tabs.vue'

export const LazyCard = import('../../components/Card.vue' /* webpackChunkName: "components/card" */).then(c => wrapFunctional(c.default || c))
export const LazyCards = import('../../components/Cards.vue' /* webpackChunkName: "components/cards" */).then(c => wrapFunctional(c.default || c))
export const LazyFooter = import('../../components/Footer.vue' /* webpackChunkName: "components/footer" */).then(c => wrapFunctional(c.default || c))
export const LazyNewAppointment = import('../../components/NewAppointment.vue' /* webpackChunkName: "components/new-appointment" */).then(c => wrapFunctional(c.default || c))
export const LazyTabs = import('../../components/Tabs.vue' /* webpackChunkName: "components/tabs" */).then(c => wrapFunctional(c.default || c))
