import Vue from 'vue'
import { wrapFunctional } from './utils'

const components = {
  Card: () => import('../../components/Card.vue' /* webpackChunkName: "components/card" */).then(c => wrapFunctional(c.default || c)),
  Cards: () => import('../../components/Cards.vue' /* webpackChunkName: "components/cards" */).then(c => wrapFunctional(c.default || c)),
  Footer: () => import('../../components/Footer.vue' /* webpackChunkName: "components/footer" */).then(c => wrapFunctional(c.default || c)),
  NewAppointment: () => import('../../components/NewAppointment.vue' /* webpackChunkName: "components/new-appointment" */).then(c => wrapFunctional(c.default || c)),
  Tabs: () => import('../../components/Tabs.vue' /* webpackChunkName: "components/tabs" */).then(c => wrapFunctional(c.default || c))
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}
