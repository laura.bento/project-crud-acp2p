# Project CRUD 
### Application made as the final project of the Vue.js module.

## 🗓 Project Description 
##### A space for you note your appointments and never miss one. In this application you can create, delete and edit your appointment, also you can see all of then to organize your day.

## 🏁 To use the application
```
# install dependencies
$ npm install

# start JSON Server
$ npm run backend

# serve with hot reload at localhost:3000
$ npm run dev
```

## 📊 Test
##### Unit test using Jest.
```
$ npm run test
```

## 💻 Technologies 
* Vue.js
* Vuex
* Nuxt
* JSON Server
* Unit Test using Jest
